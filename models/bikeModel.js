var Bike = function(id, color, model, location) {
    this.id = id;
    this.color = color;
    this.model = model;
    this.location = location;
}

Bike.prototype.toString = function() {
    return `id: ${this.id} | color: ${this.color}`
}

Bike.allBikes = [];
Bike.add = function(aBike) {
    Bike.allBikes.push(aBike);
}

Bike.findById = function(aBikeId) {
    var aBike = Bike.allBikes.find(x => x.id == aBikeId);
    if (aBike)
        return aBike;
    else
        throw new Error(`No existe una bicicleta con el id ${aBikeId}`);
}

Bike.removeById = function(aBikeId) {
    for (var i = 0; i < Bike.allBikes.length; i++) {
        if (Bike.allBikes[i].id == aBikeId) {
            Bike.allBikes.splice(i, 1);
            break;
        }
    }
}

var bike1 = new Bike(1, 'red', 'urban', [-34.6063032, -58.3752684]);
var bike2 = new Bike(2, 'blue', 'beach', [-34.5514123, -58.4701601]);

Bike.add(bike1);
Bike.add(bike2);


module.exports = Bike;