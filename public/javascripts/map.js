var mymap = L.map('main_map').setView([-34.6068419, -58.392971], 13);

L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="https://www.openstreetmap.org/copyright">OpenStreetMap</a> contributors'
}).addTo(mymap);

// L.marker([-34.6010809, -58.3886508]).addTo(mymap);
// L.marker([-34.625066, -58.4118147]).addTo(mymap);
// L.marker([-34.6057485, -58.3759991]).addTo(mymap);

$.ajax({
    dataType: "json",
    url: "api/bikes",
    success: function(result) {
        console.log(result)
        result.bikes.forEach(function(bike) {
            L.marker(bike.location, { title: bike.id }).addTo(mymap);

        });
    }
});