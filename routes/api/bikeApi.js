// Instanciamos Express
var express = require('express');

// Creamos el router
var router = express.Router();
// Seteamos el controller
var bikeController = require('../../controllers/api/bikeControllerAPI');

router.get('/', bikeController.bike_list);
router.post('/create', bikeController.bike_create);
router.delete('/delete', bikeController.bike_delete);
router.post('/update', bikeController.bike_update);




module.exports = router;