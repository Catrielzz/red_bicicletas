// Instanciamos Express
var express = require('express');

// Creamos el router
var router = express.Router();
// Seteamos el controller
var bikeController = require('../controllers/bikeController');

// Asignamos el verbo y su ruta
router.get('/', bikeController.bike_list);
router.get('/create', bikeController.bike_create_get);
router.post('/create', bikeController.bike_create_post);
router.get('/:id/update', bikeController.bike_update_get);
router.post('/:id/update', bikeController.bike_update_post);
router.post('/:id/delete', bikeController.bike_delete_post);


module.exports = router;