const BikeApi = require('../../models/bikeModel');

exports.bike_list = function(req, res) {
    res.status(200).json({
        bikes: BikeApi.allBikes
    });
}

exports.bike_create = function(req, res) {
    var bike = new BikeApi(req.body.id, req.body.color, req.body.model, req.body.location);
    bike.location = [req.body.lat, req.body.lng];

    BikeApi.add(bike);

    res.status(200).json({
        bike_bk: bike
    });
}

exports.bike_delete = function(req, res) {
    BikeApi.removeById(req.body.id);
    res.status(204).send();
}

exports.bike_update = function(req, res) {
    var bike = BikeApi.findById(req.body.id);
    bike.id = req.body.id;
    bike.color = req.body.color;
    bike.model = req.body.model;
    bike.location = [req.body.lat, req.body.lng];

    res.status(200).json({
        bike_bk: bike
    });
}